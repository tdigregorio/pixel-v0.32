if( ! Detector.webgl ) Detector.addGetWebGLMessage();

var container, stats;

var camera, scene, renderer;

var geometry, objects;

var mouseX = 0, mouseY = 0;

var cube, gravity = 0.4;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

document.addEventListener( 'mousemove', onDocumentMouseMove, false );

init();

function init() {

	// container = document.createElement( 'div' );
	// document.body.appendChild( container );

	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 15000 );
	camera.position.z = 500;

	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x000000, 1, 15000 );

	// var light = new THREE.PointLight( 0xffffff );
	// light.position.set( 100, 100, 100 );
	// scene.add( light );

	var light = new THREE.AmbientLight( 0x111111 );
	scene.add( light );

	// Cube
	cube = CubePlayer(90);
	scene.add( cube );
	
	// Level
	// scene.add( levelGroup = currentLevel );
	

	renderer = new THREE.WebGLRenderer( { alpha: false, antialias : true } );
	renderer.setClearColor( 0xFFFFFF, 1 );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.sortObjects = false;
	document.body.appendChild( renderer.domElement );

	//

	window.addEventListener( 'resize', onWindowResize, false );
	
	// cube.restart();
}

function onWindowResize() {

	windowHalfX = window.innerWidth / 2;
	windowHalfY = window.innerHeight / 2;

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

function onDocumentMouseMove(event) {

	mouseX = ( event.clientX - windowHalfX );
	mouseY = ( event.clientY - windowHalfY ) * 2;

}





window.addEventListener('keydown', function(e){
	console.log(e);
	if(e.code == "ArrowRight")
		cube.turnRight();
	if(e.code == "ArrowLeft")
		cube.turnLeft();
	if(e.code == "ArrowUp")
		cube.moveForward();
	if(e.code == "ArrowDown")
		cube.moveBackward();
	if(e.code == "Space")
		cube.jump();
});



function render() {

	// Loop animation
	requestAnimationFrame( render );
	
	// mesh.renderTweens();
	TWEEN.update();
	cube.cameraFollow();
	cube.gravity();
	//mesh.morphTargetInfluences[ 0 ] = Math.sin( mesh.rotation.y ) * 0.5 + 0.5;

	//camera.position.x += ( mouseX - camera.position.x ) * .005;
	// camera.position.y += ( - mouseY - camera.position.y ) * 01;
	cube.follow.position.y += ( - mouseY - cube.follow.position.y ) * 01;

	camera.lookAt( cube.position );

	renderer.render( scene, camera );

}
render();

