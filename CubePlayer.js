﻿(function(){

var Group = THREE.Object3D,
	Cube = function(x, y, z, mat)
	{
		mat = new THREE.MeshLambertMaterial( mat );
		mat.transparent = true;
		return new THREE.Mesh( 
			new THREE.CubeGeometry( x, y, z ),
			mat
		);
	};


window.CubePlayer = function CubePlayer(size)
{
	var light;
	
	cube = new Group();
	// Tweenable(cube);
	cube.add(
		cube.cubeInner = new Cube( size, size, size , { color: 0x8500FF } )
	);
	cube.cubeInner.scale.set(0, 0, 0);
	
	cube.follow = new Group();
	cube.follow.position.x = 200;
	cube.follow.position.y = 400;
	cube.follow.position.z = 800;
	cube.add(cube.follow);
	
	document.addEventListener('mousewheel', function(e)
	{
		console.log(e, e.wheelDelta);
		var multi = e.wheelDelta < 0 ? 2 : 0.5;
		multi = cube.follow.position.x < 200 || cube.follow.position.x > 2000 ? 0 : multi;
		cube.follow.position.multiplyScalar( multi );
	});

	// light = new THREE.AmbientLight( 0x111111 );
	// light.position.x = cube.follow.position.x;
	// light.position.y = cube.follow.position.y;
	// light.position.z = cube.follow.position.z;
	// cube.add( light );
	
	cube.light1 = new THREE.PointLight( 0xffffff );
	cube.light1.position.set( cube.follow.position.x, cube.follow.position.y, cube.follow.position.z );
	cube.light1.intensity = 0.5;
	cube.add( cube.light1 );
	
	cube.light2 = new THREE.PointLight( 0xffffff );
	cube.light2.position.set( cube.follow.position.x, cube.follow.position.y, 0 );
	cube.light2.intensity = 0.3;
	cube.add( cube.light2 );
	
	if(CubePlayer.debug)
	{
		cube.sens = new Cube( 10, 10, size * 3 , { color: 0x0000ff } );
		cube.sens.position.z = -size * 2;
		cube.add(cube.sens);
		cube.follower = new Cube( 10, 10, 10, { color: 0xffffff } );
		cube.follower.position.x = cube.follow.position.x;
		cube.follower.position.y = cube.follow.position.y;
		cube.follower.position.z = cube.follow.position.z;
		cube.add(cube.follower);
		
	}
	
	cube.active = false;
	cube.direction = 0;
	cube._moved = false;
	cube._moving = false;
	cube.cameraLattency = 1000;
	cube.moveDuration = 300;
	cube.turnDuration = 0;
	cube.jumpDuration = 300;
	cube.jumpHigh = 1;
	cube.winDuration = 1000;
	cube.dieDuration = 500;
	
	cube.levelPosition = {v: 0, w:0, h:0};
	
	cube.restart = function()
	{
		// Reset properties
		cube.cubeInner.material.opacity = 0;
		cube.position.x = currentLevel.startTile.v * 100;
		cube.position.z = - currentLevel.startTile.w * 100;
		cube.position.y = currentLevel.startTile.h * 100;
		cube.rotation.y = 0;
		cube.levelPosition = {v: currentLevel.startTile.v, w: currentLevel.startTile.w, h: currentLevel.startTile.h};
		cube._moving = false;
		cube.cameraFollow.active = true;
		
		// Start tweens
		setTimeout(function(){ currentLevel.sounds.restart.play(); }, 1000);
		
		new TWEEN.Tween( cube.cubeInner.material ).to( { opacity: 1 }, cube.dieDuration * 3 )
				.delay(1000)
				.easing(TWEEN.Easing.Elastic.Out)
				.start();
		new TWEEN.Tween( cube.cubeInner.scale ).to( {x: 1, y: 1, z: 1}, cube.dieDuration * 3 )
				.delay(1000)
				.easing(TWEEN.Easing.Elastic.Out)
				.onComplete(function()
				{
					cube.active = true;
				})
				.start();
	}
	
	cube.checkTile = function()
	{
		console.log('checkTile', cube._moved);
		
		if(!cube._moved)
			return;
		
		
		var tile = currentLevel.getObjectByName('Tile '+cube.levelPosition.v+' '+cube.levelPosition.w);
		
		
		if(!tile)
			tile = {type: ' ', high : -1};
			// return cube.die();
		
		// if(tile.type == ' ')
			// return cube.die();
		if(tile && tile.type == 'Z')
			return cube.win();
		
		if(cube.levelPosition.h < tile.high)
			return cube.die();
		else if(tile.high <= cube.levelPosition.h)
		{
			// if(cube._fallTween)
			// {
				// cube._fallTween.stop();
				// cube._fallTween = null;
			// }
			// cube.fall();
		}
		cube._moved = false;
	}
	
	
	cube.win = function()
	{
		cube.active = false;
		
		currentLevel.sounds.win.play();
		
		new TWEEN.Tween( cube.cubeInner.scale ).to( {x: 0, y: 0, z: 0}, cube.winDuration ).start();
		new TWEEN.Tween( cube.cubeInner.material ).to( { opacity: 0 }, cube.winDuration ).start();
		new TWEEN.Tween( cube.rotation ).to( {y: 2 * Math.PI}, cube.winDuration ).start();
		new TWEEN.Tween( cube.position ).to( {y: 1000}, cube.winDuration )
				.onComplete(function()
				{
					loadLevel(currentLevelID++ < levels.length - 1 ? currentLevelID : 0);
					cube.cubeInner.material.opacity = 0;
					cube.cubeInner.scale.set(0,0,0);
				})
				.start();
	}
	cube.die = function()
	{
		// if(cube._fallTween)
			// cube._fallTween.stop();
		cube._moving = true;
		cube.active = false;
		
		currentLevel.sounds.die.play();
		
		cube.cameraFollow.active = false;
		new TWEEN.Tween( cube.cubeInner.material ).to( { opacity: 0 }, cube.dieDuration )
				.start();
		new TWEEN.Tween( cube.cubeInner.scale ).to( {x: 0, y: 0, z: 0}, cube.dieDuration )
				.onComplete(function()
				{
					// cube.cameraFollow();
					cube.restart();
				})
				.start();
	}
	
	cube.jump = function()
	{
		if(!cube.active/* || cube._fallTween*/)
			return;
		var tile = currentLevel.getObjectByName('Tile '+cube.levelPosition.v+' '+cube.levelPosition.w);
		
		cube.currentGravity = - cube.jumpHigh * 15;
		cube.position.y++;
		currentLevel.sounds.jump.play();
		
		if(tile && cube.position.y < tile.position.y)
			return;
		/*
		
		
		new TWEEN.Tween( cube.position ).to( {y: size * (cube.levelPosition.h + cube.jumpHigh)}, cube.jumpDuration )
				// .easing(TWEEN.Easing.Quadratic.Out)
				.onUpdate(function()
				{
					cube.levelPosition.h = Math.ceil(cube.position.y / size);
				// console.log('cube.high:', cube.levelPosition.h);
				})
				.onComplete(function()
				{
					if(!cube._moved)
						cube.checkTile();
					// cube._moving && cube.fall();
				})
				.start();*/
	}
	cube.moveForward = function()
	{
		if(!cube.active || cube._moving) return false;
		
		cube._moving = true;
		
		currentLevel.sounds.step.play();
		
		cube.direction = cube.rotation.y / (Math.PI/2);
		var direction;
		// if(cube.direction < Math.PI) direction = 'S';
		if(cube.direction == 0) direction = 'N';
		if(cube.direction == 2) direction = 'S';
		if(cube.direction == 1) direction = 'W';
		if(cube.direction == -1) direction = 'E';

		console.log(cube.rotation.y, cube.direction, direction);
		var moveTween;
		if(direction == 'N') cube.levelPosition.w++;
		if(direction == 'S') cube.levelPosition.w--;
		if(direction == 'W') cube.levelPosition.v--;
		if(direction == 'E') cube.levelPosition.v++;
		
		moveTween = {x: cube.levelPosition.v * 100, z: cube.levelPosition.w * -100};
		// if(direction == 'N') moveTween = {z: cube.position.z - 100};
		// if(direction == 'S') moveTween = {z: cube.position.z + 100};
		// if(direction == 'W') moveTween = {x: cube.position.x - 100};
		// if(direction == 'E') moveTween = {x: cube.position.x + 100};
		
		
		
		var tween = new TWEEN.Tween( cube.cubeInner.rotation ).to( {x: - Math.PI/2}, this.moveDuration )
				.start();
		
		
		
		var tween2 = new TWEEN.Tween( cube.position ).to( moveTween, this.moveDuration )
				.onComplete(function()
				{
					cube.cubeInner.rotation.x = 0;
					// cube.cubeInner.position.z = 0;
					// console.log(cube.follow);
					
					// cube.cameraFollow();
					
					
					cube._moving = false;
					cube._moved = true;
					
					cube.checkTile();
				})
				.start();
				
	}
	
	cube.moveBackward = function()
	{
		if(!cube.active || cube._moving) return false;
		
		var sens = cube.rotation.y - Math.PI;
		sens = (sens > Math.PI) ? - Math.PI/2 : (sens <= - Math.PI) ? + Math.PI : sens;
		
		// cube.rotation.y = sens;
		var tween = new TWEEN.Tween( cube.rotation ).to( {y: sens}, cube.turnDuration )
				.onComplete(function(){cube.moveForward();})
				.start();
		
	}
	cube.turnRight = function()
	{
		if(!cube.active || cube._moving) return false;
		
		var sens = cube.rotation.y - Math.PI/2;
		sens = (sens > Math.PI) ? - Math.PI/2 : (sens <= - Math.PI) ? + Math.PI : sens;
		
		// cube.rotation.y = sens;
		var tween = new TWEEN.Tween( cube.rotation ).to( {y: sens}, cube.turnDuration )
				.onComplete(function(){cube.moveForward();})
				.start();
		
	}
	cube.turnLeft = function()
	{
		if(!cube.active || cube._moving) return false;
		
		var sens = cube.rotation.y + Math.PI/2;
		sens = (sens > Math.PI) ? - Math.PI/2 : (sens <= - Math.PI) ? + Math.PI : sens;
		
		var tween = new TWEEN.Tween( cube.rotation ).to( {y: sens}, cube.turnDuration )
				.onComplete(function(){cube.moveForward();})
				.start();
				
	}
	
	
	cube.cameraFollow = function()
	{
		if(cube.cameraFollow.active)
		{
			var global = new THREE.Vector3();
			global.setFromMatrixPosition( cube.follow.matrixWorld );
			if(!isNaN(global.x) && !isNaN(global.y) && !isNaN(global.z))
			new TWEEN.Tween( camera.position ).to( global, cube.cameraLattency ).start();
		}
	}
	
	cube.currentGravity = 0;
	cube.gravity = function()
	{
		if(cube.active)
		{
			
			var block = currentLevel.getObjectByName('Tile '+cube.levelPosition.v+' '+cube.levelPosition.w);
			if(block)
			{
				if(block.position.y - 50 < cube.position.y && cube.position.y <= block.position.y)
				{
					cube.currentGravity = 0;
					cube.position.y = block.position.y;
				}
				// if(block.position.y < cube.position.y)
			}
			
			// if(block && block.solidity)
				// cube.currentGravity -= block.solidity;
			
			// if(cube.position.y >= block.position.y)
			if(cube.position.y < -10 * Level.GRID_SIZE) // Fin du monde !
				cube.die();
			
			
			// Add the gravity
			cube.currentGravity += gravity;
			
			// Move the cube according to it's current force
			cube.position.y -= cube.currentGravity;
		}
	}
	
	
	return cube;
}
CubePlayer.debug = false;



})();