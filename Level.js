﻿var theme0 = { background: 0xffffff, cube: 0xD899FF, A: 0xffffff, B: 0xffffff, Z: 0xffffff, tile: {duration: 0, delay: 0, easing: TWEEN.Easing.Back.Out} };
var theme1 = { background: 0xC1D8DA, cube: 0x8C8559, A: 0x896A2A, B: 0x5A4A31, Z: 0xA58C6D, tile: {duration: 2000, delay: 100, easing: TWEEN.Easing.Elastic.Out} };
var theme2 = { background: 0x08275C, cube: 0x064C7B, A: 0x07BDF4, B: 0x07ECF3, Z: 0x8C7261, tile: {duration: 2000, delay: 100, easing: TWEEN.Easing.Bounce.Out} };
var theme3 = { background: 0x5D2680, cube: 0xD899FF, A: 0xBA4CFF, B: 0x6C4C80, Z: 0x953DCC, tile: {duration: 1000, delay: 100, easing: TWEEN.Easing.Back.Out} };




var sounds1 = {
			tiles: "Unlock_metroid.ogg",
			restart: "Unlock.ogg",
			step: "camera_focus.ogg",
			jump: "jump.ogg",
			fall: "KeypressSpacebar.ogg",
			win: "Victory - Metroid SMS.ogg",
			die: "Lock.ogg"
		};
		
var sounds2 = {
			tiles: "vega.ogg",
			restart: "baffe.ogg",
			step: "Argon.ogg",
			jump: "Beryllium.ogg",
			fall: "KeypressSpacebar.ogg",
			win: "allegro.ogg",
			die: "Fluorine.ogg"
		};


		


		


var levels = [
		{
			name: "Menu",
			style: theme0,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0A|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|0B|  ",
				"  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  "
			]
		},
		{
			name: "Tuto - Avancer",
			style: theme3,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  ",
				"  |0A|0B|0B|0B|0Z|  ",
				"  |  |  |  |  |  |  "
			]
		},
		{
			name: "Tuto - Tourner",
			style: theme1,
			sounds: sounds2,
			tiles: [
				"  |  |  |  |  |  |  ",
				"  |0A|0B|0B|0B|0B|  ",
				"  |  |  |  |  |0B|  ",
				"  |0B|0B|0Z|  |0B|  ",
				"  |0B|  |  |  |0B|  ",
				"  |0B|0B|  |  |0B|  ",
				"  |  |0B|0B|0B|0B|  ",
				"  |  |  |  |  |  |  "
			]
		},
		{
			name: "Tuto - Sauter 1",
			style: theme1,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  ",
				"  |0A|0B|1B|0B|0Z|  ",
				"  |  |  |  |  |  |  "
			]
		},
		{
			name: "Tuto - Sauter 2",
			style: theme2,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  ",
				"  |0A|0B|  |0B|0Z|  ",
				"  |  |  |  |  |  |  "
			]
		},
		{
			name: "Tuto  - Tous les mouvements",
			style: theme3,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  ",
				"  |2A|2B|1B|1B|1B|  ",
				"  |  |  |  |  |1B|  ",
				"  |1B|2B|3B|2B|1B|  ",
				"  |1B|  |  |  |  |  ",
				"  |1B|1B|0B|0B|0Z|  ",
				"  |  |  |  |  |  |  "
			]
		},
		{
			name: "Test - C'est pas la taille qui compte",
			style: theme3,
			sounds: sounds1,
			tiles: [
				"  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |2B|2B|2B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |2B|2B|2B|  |  |  |  |8B|9B|9B|9B|9B|  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |2B|2B|2B|  |  |  |  |8B|7B|6B|5B|9B|  |  |  |  |  |  |  |  |7B|  |  |  ",
				"  |2B|  |  |  |  |  |  |  |  |  |4B|9B|  |  |  |  |  |  |  |  |7B|  |  |  ",
				"  |2B|  |  |  |  |  |  |  |  |  |3B|9B|  |  |  |  |  |  |  |  |6B|  |  |  ",
				"  |2B|2B|2B|  |  |  |  |  |  |  |2B|9B|9B|9B|9B|8B|7B|6B|5B|5B|5B|  |  |  ",
				"  |  |  |3B|  |  |  |  |  |2B|  |1B|  |  |9B|  |  |  |  |5B|5B|5B|  |  |  ",
				"  |  |  |3B|  |3B|4B|4B|4B|1B|1B|1B|9B|9B|9B|  |  |  |  |5B|3B|5B|  |  |  ",
				"  |  |  |3B|  |3B|  |  |  |  |  |  |9B|  |  |  |  |  |  |5B|5B|5B|  |  |  ",
				"  |  |  |3B|3B|3B|  |  |  |  |  |  |9B|  |  |  |  |  |  |  |4B|  |  |  |  ",
				"  |  |  |  |  |  |  |  |  |  |  |  |9A|  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |  |  |  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |  |  |  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |8B|8B|8B|8B|8B|  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |4B|4B|4B|4B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |3B|  |  |4B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |3B|  |  |4B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |3B|  |  |5B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |3B|3B|3B|  |5B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |3B|0Z|4B|5B|6B|  |  |8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |3B|1B|4B|  |7B|8B|8B|8B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |3B|2B|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  ",
				"  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  "
			]
		}
	];




function BlockA(v, w, h, level)
{
	var color = level.style.A || 0xffffff;
	// var color = typeof group.style[type] != 'undefined' ? group.style[type] : 0xffffff, high = parseInt(tileValue) || 0;
	
	
	var block = new THREE.Object3D();
	tile = new THREE.Mesh( new THREE.CubeGeometry( 90, 10, 90 ), new THREE.MeshLambertMaterial( { color: color } ) );
	tile.position.x = 0;
	tile.position.y = -50;
	tile.position.z = 0;
	block.add( tile );
	
	block.v = v;
	block.w = w;
	block.h = h;
	
	return block;
}

function BlockB(v, w, h, level)
{
	var color = level.style.B || 0xffffff;
	
	var block = new THREE.Object3D();
	tile = new THREE.Mesh( new THREE.CubeGeometry( 90, 10, 90 ), new THREE.MeshLambertMaterial( { color: color } ) );
	tile.position.x = 0;
	tile.position.y = -50;
	tile.position.z = 0;
	block.add( tile );
	
	block.v = v;
	block.w = w;
	block.h = h;
	
	return block;
}

function BlockZ(v, w, h, level)
{
	var color = level.style.Z || 0xffffff;
	
	var block = new THREE.Object3D();
	tile = new THREE.Mesh( new THREE.CubeGeometry( 90, 10, 90 ), new THREE.MeshLambertMaterial( { color: color } ) );
	tile.position.x = 0;
	tile.position.y = -50;
	tile.position.z = 0;
	block.add( tile );
	
	block.v = v;
	block.w = w;
	block.h = h;
	
	return block;
}





(function()
{
	var Level = window.Level = function Level(json, id)
	{
		var level = new THREE.Object3D();
		level.startTile = [0,0,0];
		level.levelName = json.name;
		level.style = json.style;
		level.sounds = json.sounds;
		
		// Parses the sounds urls to transform it into a <audio id="<url>"><source src="<url>" type="audio/ogg"/></audio>
		for(var s in json.sounds)
		{
			json.sounds[s] = typeof json.sounds[s] == 'string' ? document.getElementById(json.sounds[s]) || 
							(function()
							{
								var audio = document.createElement('audio');
								audio.id = json.sounds[s];
								audio.innerHTML = '<source src="./sounds/' + json.sounds[s]+ '" type="audio/ogg"/>';
								document.body.appendChild(audio);
								return audio;
							})()
							: json.sounds[s];
			
		}
		
		// Look on Array matrix strings (2 dimenssions v x w)
		for(var v = 0, row; row = json.tiles[v]; v++)
			for(var w = 0, row; tileValue = json.tiles[v][w]; w++)
			{
				var type = tileValue[tileValue.length - 1];
				if(type != ' ')
				{
					var color = level.style[type] || 0xffffff, high = parseInt(tileValue) || 0;
					// var color = typeof level.style[type] != 'undefined' ? level.style[type] : 0xffffff, high = parseInt(tileValue) || 0;
					
					var block = window['Block'+type](v, w, high, level);
					block.name = 'Tile '+v+' '+w;
					block.type = type;
					
					// if(type == 'A') color = 0xff0000;
					// if(type == 'Z') color = 0x000000;
					// var block = new THREE.Object3D();
					// tile = new THREE.Mesh( new THREE.CubeGeometry( 90, 10, 90 ), new THREE.MeshLambertMaterial( { color: color } ) );
					// tile.position.x = 0;
					// tile.position.y = -50;
					// tile.position.z = 0;
					
					block.position.x = v * Level.GRID_SIZE;
					block.position.z = -w * Level.GRID_SIZE;
					// if(tileValue == '2') high = 1;
					block.position.y = -55 + (high * Level.GRID_SIZE);
					
					
					// block.add( tile );
					level.add( block );
					
					if(type == 'A')
						level.startTile = {v: v, w:w, h: high};
				}
			}
		
		// Render HTML Buttons to switch levels
		var levelButton = document.createElement('button');
		levelButton.innerHTML = level.levelName;
		levelButton.levelID = id;
		levelButton.onclick = function(e)
		{
			console.log(levelButton, id);
			loadLevel(levelButton.levelID);
		};
		document.querySelector('#levels').appendChild(levelButton);
		
		return level;
	}
	Level.GRID_SIZE = 100;
	
	function createLevel(level, id)
	{
		var group = new THREE.Object3D();//create an empty container
		group.startTile = [0,0,0];
		group.levelName = level.name;
		group.style = level.style;
		
		group.sounds = level.sounds;
		for(var s in group.sounds)
		{
			//<audio id="Unlock"><source src="Unlock.ogg" type="audio/ogg"/></audio>
			var audio = typeof group.sounds[s] == 'string' ? document.getElementById(group.sounds[s]) || 
							(function()
							{
								var audio = document.createElement('audio');
								audio.id = group.sounds[s];
								var source = document.createElement('source');
								source.type = "audio/ogg"
								source.src = './sounds/' + group.sounds[s];
								audio.appendChild(source);
								document.body.appendChild(audio);
								return audio;
							})()
							: group.sounds[s];
			
			group.sounds[s] = audio;
		}
		
		
		for(var v = 0, row; row = level.tiles[v]; v++)
			for(var w = 0, row; tileValue = level.tiles[v][w]; w++)
			{
				var type = tileValue[tileValue.length - 1];
				if(type != ' ')
				{
					var color = typeof group.style[type] != 'undefined' ? group.style[type] : 0xffffff, high = parseInt(tileValue) || 0;
					// if(type == 'A') color = 0xff0000;
					// if(type == 'Z') color = 0x000000;
					var block = new THREE.Object3D();
					tile = new THREE.Mesh( new THREE.CubeGeometry( 90, 10, 90 ), new THREE.MeshLambertMaterial( { color: color } ) );
					tile.position.x = v*100;
					tile.position.z = -w*100;
					// if(tileValue == '2') high = 1;
					tile.position.y = -55 + (high * 100);
					tile.name = 'Tile '+v+' '+w;
					tile.type = type;
					tile.high = high;
					block.add( tile );
					group.add( block );
					
					if(type == 'A')
						group.startTile = [v, w, high];
				}
			}
		
		// Render HTML
		var levelButton = document.createElement('button');
		levelButton.innerHTML = level.name;
		levelButton.levelID = id;
		levelButton.onclick = function(e)
		{
			console.log(levelButton);
			loadLevel(levelButton.levelID);
		};
		document.querySelector('#levels').appendChild(levelButton);
		
		return group;
	}
	
	for(var l = 0, level; level = levels[l]; l++)
	{
		for(var r = 0, row; row = levels[l].tiles[r]; r++)
			levels[l].tiles[r] = levels[l].tiles[r].split('|');
		
		// levels[l] = createLevel(level, l);
		levels[l] = new Level(level, l);
	}


})();

var currentLevelID = 0,
	tileAnimDuration = 2000;
	tileAnimDelay = 100;
    currentLevel = null;

function loadLevel(id)
{
	if(currentLevel)
	{
		cube.die();
		scene.remove(currentLevel);
	}
	
	if(levels[id])
	{
		currentLevelID = id;
		currentLevel = levels[id];
		scene.add(currentLevel);
		renderer.setClearColor( currentLevel.style.background, 1 );
		cube.cubeInner.material.color.setHex(currentLevel.style.cube);
		cube.cubeInner.material.opacity = 0;
		
		cube.position.x = currentLevel.startTile.v * 100;
		cube.position.z = - currentLevel.startTile.w * 100;
		cube.position.y = currentLevel.startTile.h * 100;
		
		var stile = currentLevel.style.tile;
		currentLevel.sounds.tiles.play();
		currentLevel.children.forEach(function (tile, i)
		{
			tile.position.y = tile.position.y + 1000;
			var tween = new TWEEN.Tween( tile.position ).to( {y: tile.position.y - 1000}, stile.duration )
					.easing(stile.easing)
					.delay(i * stile.delay)
					.onComplete(function(){})
					.start();
		});
		
		
		setTimeout(cube.restart.bind(cube), stile.duration + (currentLevel.children.length * stile.delay));
	}
}



